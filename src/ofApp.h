
#include "ofMain.h"
#include "ofxOpenCv.h"
#include "noiseField.h"
#include "ofxGui.h"
#include "ofxKinect.h"
#include "ofxOsc.h"

#include "particle.h"
//#include "squareParticle.h"
//#include "circleParticle.h"

#define _USE_WEB_CAM

class ofApp : public ofBaseApp {
public:
  enum AppMode { MODE_PARTICLES, MODE_LINES };

  void setup();
  void update();
  void draw();

  // glm::vec2 getOpticalFlowValueForPercent(float xpct, float ypct);

  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y);
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseEntered(int x, int y);
  void mouseExited(int x, int y);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);


  //------KINECT OR CAMERA---------

  #ifdef _USE_WEB_CAM
  
  ofVideoGrabber video;

  #else

  ofxKinect kinect;
  int angle;

  #endif
  
  //---------SOURCE_IMAGE--------------

  int sourceWidth, sourceHeight;

  //---------PARTICLE SYSTEM----------
  
  vector <particle * > particles;
  vector <particle * > yellowLeafs;
  vector <particle * > secas;
  vector <particle * > helechos;
  vector <particle * > araucarias;
  
  noiseField NF;

  bool bAttract;  
  bool bPressed;

  void populateVector(std::vector<particle * > _particles, std::string _path, int _amountParticles, float _damping);


  //---VIDEO BACKGROUND
  
  ofVideoPlayer videoPlayer;

  
  //-------OPENCV----------------

  ofxCvColorImage videoCvColor;
  ofxCvGrayscaleImage videoCvGray;
  ofxCvGrayscaleImage videoPrevFrame;
  ofxCvGrayscaleImage videoDiffImage;
  ofxCvGrayscaleImage background;

  ofxCvContourFinder contourFinder;

  ofxCvGrayscaleImage grayThreshNear; // the near thresholded image
  ofxCvGrayscaleImage grayThreshFar; // the far thresholded image
	

  bool bLearnBackground;

  std::vector<glm::vec2> centroids;
  std::vector<float> blobAreas;

  //-------OSC--------------

  ofxOscSender sender;
  //ofxOscReceiver receiver;

  void sendOsc();

  //-------GUI--------------
  
  ofxPanel gui;

  ofxGuiGroup showImage;
  
  ofParameter<int> blurAmount;
  ofParameter<int> threshold;
  ofParameter<int> maxBlobSize;
  ofParameter<int> minBlobSize;
  ofParameter<int> nBlobsConsidered;
  ofParameter<int> nearThreshold;
  ofParameter<int> farThreshold;
  ofParameter<bool> bThreshWithOpenCV;
  ofParameter<int> nErosions;
  ofParameter<int> nDilations;

  ofParameter<float> scaleForceRepulsion;
  ofParameter<float> repulsionZone;
  
  ofParameter<bool> bVideoDiff;
  ofParameter<bool> bBlobsContourns;
  ofParameter<bool> bParticles;
  ofParameter<bool> bVideoGray;
  ofParameter<bool> bVideoColor;
  ofParameter<bool> bVideoBackground;
  ofParameter<bool> bFrezzeImage;
  
  bool bGui;

};
