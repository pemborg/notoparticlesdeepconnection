#ifndef PARTICLE_H
#define PARTICLE_H

#include "ofMain.h"

class particle
{
 public:

  float drag; 
  float uniqueVal;
  float scale;


  
  glm::vec2 pos;
  glm::vec2 initialPos;
  glm::vec2 vel;
  glm::vec2 frc;
  glm::vec2 basePos; // frc is also know as acceleration (newton says "f=ma")

  //  ofColor color;
  ofImage image;
  ofDirectory dir;

  float previousPosX;
  float previousPosY;

  
  particle();
  virtual ~particle(){};

  void resetForce();
  void addForce(float x, float y);
  void addRepulsionForce(float x, float y, float radius, float scale);
  void addAttractionForce(float x, float y, float radius, float scale);
  void addRepulsionForceParticles(particle * p, float radius, float scale);
  void addAttractionForceParticles(particle * p, float radius, float scale);
  void addReturnForce();
  void particleReset();
  //void addRepulsion(glm::vec2 repulsionPoint);

  
  void addRandomForce(float x, float y, float radius, float scale);
  void addGravityForce(float force, float mass);
  void addDampingForce();

  void addAngularMotion();
  
  void setInitialCondition(float px, float py, float vx, float vy);
  void update();
  
  virtual void draw();

  virtual void loadImage(std::string str);
  
  void bounceOffWalls();
	

  float size;
  float damping;


  float startTime;
  float duration;
  bool timerEnd;
  bool isStill;

  
  //float size = 5.0;
   //  float timeNotTouched = 0.0f;
   //bool bAtBasePos = false;

  //  bool bDidICollide = false;

  
  
 protected:
 private:

  float angle;
  float angularVelocity;
  
};

#endif // PARTICLE_H
