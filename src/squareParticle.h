#ifndef SQUAREPARTICLE_H
#define SQUAREPARTICLE_H

#include "ofMain.h"
#include "particle.h"

class squareParticle : public particle {

public:

  ofImage image;
  ofDirectory dir;
  //  vector<ofImage> particleImages;

  void loadImage(std::string str);
  void draw();

};

#endif
