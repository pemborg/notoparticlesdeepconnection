#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {

  ofSetVerticalSync(true);
  ofSetFrameRate(60);

  int screenWidth = ofGetWidth();
  int screenHeight = ofGetHeight();

#ifdef _USE_WEB_CAM

  //  video.initGrabber(1280, 720);
  video.setVerbose(true);
  video.setup(ofGetWidth(), ofGetHeight());

#else

  // KINECT SETUP

  // enable depth->video image calibration
  kinect.setRegistration(true);

  kinect.init();

  // opens first available kinect
  kinect.open();

  // print the intrinsic IR sensor values
  if (kinect.isConnected()) {
    ofLogNotice() << "sensor-emitter dist: "
                  << kinect.getSensorEmitterDistance() << "cm";
    ofLogNotice() << "sensor-camera dist:  " << kinect.getSensorCameraDistance()
                  << "cm";
    ofLogNotice() << "zero plane pixel size: " << kinect.getZeroPlanePixelSize()
                  << "mm";
    ofLogNotice() << "zero plane dist: " << kinect.getZeroPlaneDistance()
                  << "mm";
  }

  // zero the tilt on startup
  angle = 0;
  kinect.setCameraTiltAngle(angle);

#endif

  // VIDEO SETUP

#ifdef _USE_WEB_CAM

  videoCvColor.allocate(ofGetWidth(), ofGetHeight());
  videoCvGray.allocate(ofGetWidth(), ofGetHeight());

#else
  
  videoCvGray.allocate(kinect.getWidth(), kinect.getHeight());

#endif
  
  videoPrevFrame.allocate(ofGetWidth(), ofGetHeight());
  videoDiffImage.allocate(ofGetWidth(), ofGetHeight());
  background.allocate(ofGetWidth(), ofGetHeight());

  // videoCvColor.setFromPixels(video.getPixels());

  bLearnBackground = false;

  // PARTICLE

  bAttract = false;
  repulsionZone = 100;

  for (unsigned int i = 0; i < 500; i++) {

    //    particle * aParticle = new squareParticle();
    particle *aParticle = new particle();
    string str = "images/amarillas/";
    float randomTemp = ofRandom(0.1, 1); // change size particle
    aParticle->size = randomTemp;
    aParticle->loadImage(str);
    aParticle->setInitialCondition(ofRandom(-100, ofGetWidth()),
                                   ofRandom(-100, ofGetHeight()), 0, 0);
    //  aParticle->color = ofColor(ofMap(randomTemp, 0.1, 3, 0, 127),
    //  ofMap(randomTemp, 0.1, 3, 255, 0), ofMap(randomTemp, 0.1, 3, 0, 127));
    aParticle->basePos = aParticle->pos;
    aParticle->damping = 0.1;
    particles.push_back(aParticle);
  }

  // YELLOW LEAFS POPULATION

  for (unsigned int i = 0; i < 500; i++) {

    //    particle * aParticle = new squareParticle();
    particle *aParticle = new particle();
    string str = "images/secas/";
    float randomTemp = ofRandom(0.1, 1); // change size particle
    aParticle->size = randomTemp;
    aParticle->loadImage(str);
    aParticle->setInitialCondition(ofRandom(-100, ofGetWidth()),
                                   ofRandom(-100, ofGetHeight()), 0, 0);
    // aParticle->color = ofColor(ofMap(randomTemp, 0.1, 3, 0, 127),
    // ofMap(randomTemp, 0.1, 3, 255, 0), ofMap(randomTemp, 0.1, 3, 0, 127));
    aParticle->basePos = aParticle->pos;
    aParticle->damping = 0.1;
    secas.push_back(aParticle);
  }

  // HELECHOS POP

  for (unsigned int i = 0; i < 500; i++) {

    //    particle * aParticle = new squareParticle();
    particle *aParticle = new particle();
    string str = "images/helechos/";
    float randomTemp = ofRandom(0.1, 1); // change size particle
    aParticle->size = randomTemp;
    aParticle->loadImage(str);
    aParticle->setInitialCondition(ofRandom(-100, ofGetWidth()),
                                   ofRandom(-100, ofGetHeight()), 0, 0);
    // aParticle->color = ofColor(ofMap(randomTemp, 0.1, 3, 0, 127),
    // ofMap(randomTemp, 0.1, 3, 255, 0), ofMap(randomTemp, 0.1, 3, 0, 127));
    aParticle->basePos = aParticle->pos;
    aParticle->damping = 0.1;
    helechos.push_back(aParticle);
  }

  // ARAUCARIAS POP

  for (unsigned int i = 0; i < 50; i++) {

    //    particle * aParticle = new squareParticle();
    particle *aParticle = new particle();
    string str = "images/araucaria/";
    float randomTemp = ofRandom(0.1, 1); // change size particle
    aParticle->size = randomTemp;
    aParticle->loadImage(str);
    aParticle->setInitialCondition(ofRandom(-100, ofGetWidth()),
                                   ofRandom(-100, ofGetHeight()), 0, 0);
    // aParticle->color = ofColor(ofMap(randomTemp, 0.1, 3, 0, 127),
    // ofMap(randomTemp, 0.1, 3, 255, 0), ofMap(randomTemp, 0.1, 3, 0, 127));
    aParticle->basePos = aParticle->pos;
    aParticle->damping = 0.1;
    araucarias.push_back(aParticle);
  }

  //--VIDEO BACKGROUND SETUP

  videoPlayer.load("video.mov");
  videoPlayer.setLoopState(OF_LOOP_NORMAL);
  videoPlayer.play();
  
  //----OSC SETUP
  
  sender.setup("localhost", 12345);
  // receiver.setup(12345);

  //----GUI SETUP

  gui.setup();
  gui.add(blurAmount.set("Blur", 3, 0, 24));
  gui.add(threshold.set("Threshold", 0, 0, 255));
  gui.add(minBlobSize.set("Minimun Area Blobs", 20, 0,
                          ofGetWidth() * ofGetHeight()));
  gui.add(maxBlobSize.set("Maximun Area Blobs", 25000, 0,
                          ofGetWidth() * ofGetHeight()));
  gui.add(nBlobsConsidered.set("Amount of Blobs considered", 1, 0, 20));
  gui.add(nearThreshold.set("Near Threshold kinect", 255, 0, 255));
  gui.add(farThreshold.set("Far Threshold kinect", 0, 0, 255));
  gui.add(bThreshWithOpenCV.set("Near Far Pixel/openCV", false, true, false));
  gui.add(nDilations.set("ditale", 2, 0, 20));
  gui.add(nErosions.set("erotion", 0, 0, 20));
  gui.add(scaleForceRepulsion.set("repulsion magnituted", 10, 1, 200));
  gui.add(repulsionZone.set("repulsion zone", 100, 1, 1000));

  //  gui.add(showImage.add("Monitor", ));

  gui.add(bVideoDiff.set("show Video Diff", true, true, false));
  gui.add(bBlobsContourns.set("show contourns blobs", true, true, false));
  gui.add(bParticles.set("show particles", true, true, false));
  gui.add(bVideoGray.set("show video gray colorspace", false, true, false));
  gui.add(bVideoColor.set("show video color colorspace", false, true, false));
  gui.add(bVideoBackground.set("show video background", false, true, false));
  gui.add(bFrezzeImage.set("show background", false, true, false));

  gui.loadFromFile("settings.xml");

  bGui = true;
}

//--------------------------------------------------------------

void ofApp::update() {

  int oddBlurValue = (int)blurAmount;

  bool isNewFrame = false;

  videoPlayer.update();
  

#ifdef _USE_WEB_CAM

  video.update();
  isNewFrame = video.isFrameNew();

#else

  kinect.update();
  isNewFrame = kinect.isFrameNew();

#endif

  if (isNewFrame) {

#ifdef _USE_WEB_CAM

    videoCvColor.setFromPixels(video.getPixels());
    videoCvGray = videoCvColor;
#else

    videoCvColor.setFromPixels(kinect.getPixels());
    videoCvGray.setFromPixels(kinect.getDepthPixels());
    // videoCvGray.scale(ofGetWidth(), ofGetHeight());

    if (bVideoColor) {

  videoCvColor.mirror(false, true);
  videoCvColor.resize(ofGetWidth(), ofGetHeight());

 }


#endif

#ifndef _USE_WEB_CAM

    // APPLYING THE FAR AND NEAR THRESHOLD

    if (bThreshWithOpenCV) {
      grayThreshNear = videoCvGray;
      grayThreshFar = videoCvGray;
      grayThreshNear.threshold(nearThreshold, true);
      grayThreshFar.threshold(farThreshold);
      cvAnd(grayThreshNear.getCvImage(), grayThreshFar.getCvImage(),
            videoCvGray.getCvImage(), NULL);
    } else {

      // or we do it ourselves - show people how they can work with the pixels
      ofPixels &pix = videoCvGray.getPixels();
      int numPixels = pix.size();
      for (int i = 0; i < numPixels; i++) {
        if (pix[i] < nearThreshold && pix[i] > farThreshold) {
          pix[i] = 255;
        } else {
          pix[i] = 0;
        }
      }
    }

    videoCvGray.flagImageChanged();

#endif

    int blur = blurAmount % 2;
    videoCvGray.blurGaussian(blur); // add bluer

    // resize videoGray

    videoCvGray.mirror(false, true);
    videoCvGray.resize(ofGetWidth(), ofGetHeight());

    // LEARN BACKGROUND

    if (bLearnBackground) { // learn frame
      background = videoCvGray;
      bLearnBackground = false;
    }

    videoDiffImage.absDiff(videoCvGray, background); // difference

    int maxOperations = MAX(nDilations, nErosions);

    for (int i = 0; i < maxOperations; i++) {
      if (i < nErosions)
        videoDiffImage.erode();
      if (i < nDilations)
        videoDiffImage.dilate();
    }

    videoDiffImage.blurGaussian(blurAmount); // add bluer
    videoDiffImage.threshold(threshold);     // threshold

    // CONTOUR FINDER

    contourFinder.findContours(videoDiffImage, minBlobSize, maxBlobSize,
                               nBlobsConsidered, false);

    // ADD CENTOIDS TO VECTOR CENTROIDS, AND AREA OF BLOBS TO A VECTOR AREA

    centroids.clear();
    int numBlobs = contourFinder.nBlobs;
    for (int i = 0; i < contourFinder.nBlobs; i++) {

      ofPolyline contourLines;
      contourLines = contourFinder.blobs[i].pts;

      // add centreoids coordinas to a vector
      glm::vec2 center = glm::vec2(contourFinder.blobs[i].centroid.x,
                                   contourFinder.blobs[i].centroid.y);
      centroids.push_back(center);

      // add the are of the blobs to a vector
      float area = contourFinder.blobs[i].area;
      blobAreas.push_back(area);
    }

    // PARTICLE 1

    NF.setTime(ofGetElapsedTimef() *
               0.1); // check the scalar to the speed of the animation

    // PARTICLE 2 POINTERS

    for (int i = 0; i < particles.size(); i++) {
      particles[i]->resetForce();
      if (bAttract) {
        glm::vec2 frcFromNoise = NF.getNoiseForPosition(
            particles[i]->pos.x, particles[i]->pos.y, 0.2);
        particles[i]->addForce(frcFromNoise.x, frcFromNoise.y);
        for (glm::vec2 &center : centroids) {
          particles[i]->addAttractionForce(center.x, center.y, 2000, 0.5);
        }
      }

      if (!bAttract) {
        for (glm::vec2 &center : centroids) {
	  particles[i]->addRepulsionForce(center.x, center.y, repulsionZone,
                                          scaleForceRepulsion);
        }
      }

      particles[i]->addAngularMotion();
      particles[i]->bounceOffWalls();
      particles[i]->addReturnForce();
      particles[i]->addDampingForce();
      particles[i]->update();
    }
  }

  // PARTICLE SECAS

  for (int i = 0; i < secas.size(); i++) {
    secas[i]->resetForce();
    if (bAttract) {
      glm::vec2 frcFromNoise =
          NF.getNoiseForPosition(secas[i]->pos.x, secas[i]->pos.y, 0.05);
      secas[i]->addForce(frcFromNoise.x, frcFromNoise.y);
      for (glm::vec2 &center : centroids) {
        secas[i]->addAttractionForce(center.x, center.y, 2000, 0.5);
      }
    }

    secas[i]->addAngularMotion();
    if (!bAttract) {
      for (glm::vec2 &center : centroids) {
        secas[i]->addRepulsionForce(center.x, center.y, repulsionZone,
                                    scaleForceRepulsion);
      }
    }

    secas[i]->bounceOffWalls();
    secas[i]->addReturnForce();
    secas[i]->addDampingForce();
    secas[i]->update();
  }

// Helechos

  for (int i = 0; i < helechos.size(); i++) {
    helechos[i]->resetForce();
    if (bAttract) {
      glm::vec2 frcFromNoise =
          NF.getNoiseForPosition(helechos[i]->pos.x, helechos[i]->pos.y, 0.05);
      helechos[i]->addForce(frcFromNoise.x, frcFromNoise.y);
      for (glm::vec2 &center : centroids) {
        helechos[i]->addAttractionForce(center.x, center.y, 2000, 0.5);
      }
    }

    helechos[i]->addAngularMotion();
    if (!bAttract) {
      for (glm::vec2 &center : centroids) {
        helechos[i]->addRepulsionForce(center.x, center.y, repulsionZone,
                                    scaleForceRepulsion);
      }
    }

    helechos[i]->bounceOffWalls();
    helechos[i]->addReturnForce();
    helechos[i]->addDampingForce();
    helechos[i]->update();
  }

  // ARAUCARIAS

  for (int i = 0; i < araucarias.size(); i++) {
    araucarias[i]->resetForce();
    if (bAttract) {
      glm::vec2 frcFromNoise =
          NF.getNoiseForPosition(araucarias[i]->pos.x, araucarias[i]->pos.y, 0.05);
      araucarias[i]->addForce(frcFromNoise.x, frcFromNoise.y);
      for (glm::vec2 &center : centroids) {
        araucarias[i]->addAttractionForce(center.x, center.y, 2000, 0.5);
      }
    }

    araucarias[i]->addAngularMotion();
    if (!bAttract) {
      for (glm::vec2 &center : centroids) {
        araucarias[i]->addRepulsionForce(center.x, center.y, repulsionZone,
                                    scaleForceRepulsion);
      }
    }

    araucarias[i]->bounceOffWalls();
    araucarias[i]->addReturnForce();
    araucarias[i]->addDampingForce();
    araucarias[i]->update();
  }


  
  // SEND FLOWFORCECENTERS THROUGH OSC

  sendOsc();
}

//--------------------------------------------------------------

void ofApp::draw() {

  ofSetBackgroundColor(0);

  if (bVideoBackground){
    bVideoDiff = false;
  
    videoPlayer.draw(0, 0, ofGetWidth(), ofGetHeight());
  }
  if (bVideoDiff)
    videoDiffImage.draw(0, 0);
  if (bVideoGray)
    videoCvGray.draw(0, 0);
  if (bVideoColor)
    videoCvColor.draw(0, 0);
  if (bFrezzeImage)
    background.draw(0, 0);

  if (bBlobsContourns) {

    for (int i = 0; i < contourFinder.nBlobs; i++) {
      contourFinder.blobs[i].draw(0, 0);
    }
  }

  // NF.draw();

  ofEnableAlphaBlending();

  // DRAW PARTICLES

if (bParticles) {


  
  for (int i = 0; i < helechos.size(); i++) {
    helechos[i]->draw();
  }
  
  for (int i = 0; i < particles.size(); i++) {
    particles[i]->draw();
  }

  for (int i = 0; i < secas.size(); i++) {
    secas[i]->draw();
  }

  for (int i = 0; i < araucarias.size(); i++) {
    araucarias[i]->draw();
  }
  
 }

  
  if (bGui)
    gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

  if (key == 'a')
    bAttract = !bAttract;
  if (key == 'g')
    bGui = !bGui;
  if (key == ' ') {
    bLearnBackground ? bLearnBackground == false : bLearnBackground = true;
  }

  if (key == '>' || key == '.') {
    farThreshold++;
    if (farThreshold > 255)
      farThreshold = 255;
  }

  if (key == '<' || key == ',') {
    farThreshold--;
    if (farThreshold < 0)
      farThreshold = 0;
  }

  if (key == '+' || key == '=') {
    nearThreshold++;
    if (nearThreshold > 255)
      farThreshold = 255;
  }

  if (key == '-' || key == '_') {
    nearThreshold--;
    if (nearThreshold < 0)
      farThreshold = 0;
  }

#ifndef _USE_WEB_CAM

  if (key == OF_KEY_UP) {
    angle++;
    if (angle > 30) {
      kinect.setCameraTiltAngle(angle);
    }
  }

  if (key == OF_KEY_DOWN) {
    angle--;
    if (angle < 30) {
      kinect.setCameraTiltAngle(angle);
    }
  }

#endif
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) { bPressed = true; }

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) { bPressed = false; }

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {}

//-----------OSC SEND FUNCTION

void ofApp::sendOsc() {

  ofxOscMessage oscMessage;

  for (int i = 0; i < centroids.size(); i++) {
    switch (i) {

    case 0:
      if (centroids.size() >= 0) {
        oscMessage.setAddress("/messages/centroid/0/x");
        oscMessage.addFloatArg(centroids[0].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/0/y");
        oscMessage.addFloatArg(centroids[0].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 1:
      if (centroids.size() >= 1) {
        oscMessage.setAddress("/messages/centroid/1/x");
        oscMessage.addFloatArg(centroids[1].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/1/y");
        oscMessage.addFloatArg(centroids[1].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 2:
      if (centroids.size() >= 2) {
        oscMessage.setAddress("/messages/centroid/2/x");
        oscMessage.addFloatArg(centroids[2].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/2/y");
        oscMessage.addFloatArg(centroids[2].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 3:
      if (centroids.size() >= 3) {
        oscMessage.setAddress("/messages/centroid/3/x");
        oscMessage.addFloatArg(centroids[3].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/3/y");
        oscMessage.addFloatArg(centroids[3].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 4:
      if (centroids.size() >= 4) {
        oscMessage.setAddress("/messages/centroid/4/x");
        oscMessage.addFloatArg(centroids[4].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/4/y");
        oscMessage.addFloatArg(centroids[4].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 5:
      if (centroids.size() >= 5) {
        oscMessage.setAddress("/messages/centroid/5/x");
        oscMessage.addFloatArg(centroids[5].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/5/y");
        oscMessage.addFloatArg(centroids[5].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 6:
      if (centroids.size() >= 6) {
        oscMessage.setAddress("/messages/centroid/6/x");
        oscMessage.addFloatArg(centroids[6].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/6/y");
        oscMessage.addFloatArg(centroids[6].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 7:
      if (centroids.size() >= 7) {
        oscMessage.setAddress("/messages/centroid/7/x");
        oscMessage.addFloatArg(centroids[7].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/7/y");
        oscMessage.addFloatArg(centroids[7].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 8:
      if (centroids.size() >= 8) {
        oscMessage.setAddress("/messages/centroid/8/x");
        oscMessage.addFloatArg(centroids[8].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/8/y");
        oscMessage.addFloatArg(centroids[8].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 9:
      if (centroids.size() >= 9) {
        oscMessage.setAddress("/messages/centroid/9/x");
        oscMessage.addFloatArg(centroids[9].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/9/y");
        oscMessage.addFloatArg(centroids[9].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 10:
      if (centroids.size() >= 10) {
        oscMessage.setAddress("/messages/centroid/10/x");
        oscMessage.addFloatArg(centroids[10].x);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
        oscMessage.setAddress("/messages/centroid/10/y");
        oscMessage.addFloatArg(centroids[10].y);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    }
}

    for (int j = 0; j < blobAreas.size(); j++) {
    switch (j) {

    case 0:
      if (blobAreas.size() >= 0) {
        oscMessage.setAddress("/messages/area/0");
        oscMessage.addFloatArg(blobAreas[0]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 1:
      if (blobAreas.size() >= 1) {
        oscMessage.setAddress("/messages/area/1");
        oscMessage.addFloatArg(blobAreas[1]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 2:
      if (blobAreas.size() >= 2) {
        oscMessage.setAddress("/messages/area/1");
        oscMessage.addFloatArg(blobAreas[1]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 3:
      if (blobAreas.size() >= 3) {
        oscMessage.setAddress("/messages/area/3");
        oscMessage.addFloatArg(blobAreas[3]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 4:
      if (blobAreas.size() >= 4) {
        oscMessage.setAddress("/messages/area/4");
        oscMessage.addFloatArg(blobAreas[4]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 5:
      if (blobAreas.size() >= 5) {
        oscMessage.setAddress("/messages/area/5");
        oscMessage.addFloatArg(blobAreas[5]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 6:
      if (blobAreas.size() >= 6) {
        oscMessage.setAddress("/messages/area/6");
        oscMessage.addFloatArg(blobAreas[6]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 7:
      if (blobAreas.size() >= 7) {
        oscMessage.setAddress("/messages/area/7");
        oscMessage.addFloatArg(blobAreas[7]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 8:
      if (blobAreas.size() >= 8) {
        oscMessage.setAddress("/messages/area/8");
        oscMessage.addFloatArg(blobAreas[8]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 9:
      if (blobAreas.size() >= 9) {
        oscMessage.setAddress("/messages/area/9");
        oscMessage.addFloatArg(blobAreas[9]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    case 10:
      if (blobAreas.size() >= 10) {
        oscMessage.setAddress("/messages/area/10");
        oscMessage.addFloatArg(blobAreas[10]);
        sender.sendMessage(oscMessage, false);
        //  sender.clear();
      }
      break;
    }
    }
}

void ofApp::populateVector(std::vector<particle * > _particles, std::string _path, int _amountParticles, float _damping){

for (unsigned int i = 0; i < _amountParticles; i++) {

    //    particle * aParticle = new squareParticle();
    particle *aParticle = new particle();
    string str = _path;
    float randomTemp = ofRandom(0.1, 1); // change size particle
    aParticle->size = randomTemp;
    aParticle->loadImage(str);
    aParticle->setInitialCondition(ofRandom(0, ofGetWidth()),
                                   ofRandom(0, ofGetHeight()), 0, 0);
    aParticle->basePos = aParticle->pos;
    aParticle->damping = 0.1;
    _particles.push_back(aParticle);
  }







}
