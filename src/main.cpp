#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){

	//Use ofGLFWWindowSettings for more options like multi-monitor fullscreen
	ofGLWindowSettings settings;
	settings.setSize(640, 480);
	//settings.setPosition(ofVec2f(1920, 0)); 
	//	settings.windowMode = OF_WINDOW; //can also be OF_FULLSCREEN
	settings.windowMode = OF_FULLSCREEN; //can also be OF_FULLSCREEN
	//settings.windowMode = ofWindowMode::OF_FULLSCREEN; //can also be OF_FULLSCREEN

	auto window = ofCreateWindow(settings);


	ofRunApp(window, make_shared<ofApp>());
	ofRunMainLoop();

}
