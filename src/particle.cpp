#include "particle.h"
#include "ofMain.h"

particle::particle(){
	setInitialCondition(0,0,0,0);
	damping = 0.01f; //0.08
	startTime = ofGetElapsedTimeMillis();
}

void particle::resetForce(){
    frc = glm::vec2(0, 0); // we reset the forces every frame
}

//------------------------------------------------------------
void particle::addForce(float x, float y){
    frc.x += x;
    frc.y += y;
}

//------------------------------------------------------------
void particle::addRepulsionForce(float x, float y, float radius, float scale){
    
  // (1) make a vector of where this position is: 
	
  glm::vec2 posOfForce;
  posOfForce = glm::vec2(x, y);
	
  // ----------- (2) calculate the difference & length 

  glm::vec2 diff;
  diff = pos - posOfForce;
  float length = glm::length(diff);
	
  // ----------- (3) check close enough
	
  bool bAmCloseEnough = true;
  if (radius > 0){
    if (length > radius){
      bAmCloseEnough = false;
    }
  }
	
  // ----------- (4) if so, update force
    
  if (bAmCloseEnough == true){
    float pct = 1 - (length / radius);  // stronger on the inside
    glm::vec2 nDiff  = glm::normalize(diff);
    frc.x = frc.x + nDiff.x * scale * pct;
    frc.y = frc.y + nDiff.y * scale * pct;
    }
}

//------------------------------------------------------------
void particle::addAttractionForce(float x, float y, float radius, float scale){
    
  // ----------- (1) make a vector of where this position is: 

  glm::vec2 posOfForce;
  posOfForce.x = x;
  posOfForce.y = y;

  // ----------- (2) calculate the difference & length 
	
  glm::vec2 diff;
  diff = pos - posOfForce;
  float length	= glm::length(diff);
	
  // ----------- (3) check close enough
  
  bool bAmCloseEnough = true;
  if (radius > 0){

    if (length > radius){
      bAmCloseEnough = false;
    }
  }
	
  // ----------- (4) if so, update force
    
  if (bAmCloseEnough == true){
    float pct = 1 - (length / radius);  // stronger on the inside
    glm::vec2 nDiff = glm::normalize(diff);
    frc.x = frc.x - nDiff.x * scale * pct;
    frc.y = frc.y - nDiff.y * scale * pct;
    }
}


//------------------------------------------------------------
void particle::addRepulsionForceParticles(particle * p, float radius, float scale){
	
  // ----------- (1) make a vector of where this particle p is: 
  glm::vec2 posOfForce;
  posOfForce = glm::vec2(p->pos.x, p->pos.y);	

  // ----------- (2) calculate the difference & length 
	
  glm::vec2 diff = pos - posOfForce;
  float length = glm::length(diff);
	
  // ----------- (3) check close enough
	
  bool bAmCloseEnough = true;
  if (radius > 0){
    if (length > radius){
      bAmCloseEnough = false;
    }
  }
	
  // ----------- (4) if so, update force

  float timer = ofGetElapsedTimef() - startTime;
  
  if (bAmCloseEnough == true){
    float pct = 1 - (length / radius);  // stronger on the inside
    glm::vec2 nDiff = glm::normalize(diff);
    frc.x = frc.x + nDiff.x * scale * pct;
    frc.y = frc.y + nDiff.y * scale * pct;
    p->frc.x = p->frc.x - nDiff.x * scale * pct;
    p->frc.y = p->frc.y - nDiff.y * scale * pct;

    //si la fuerza = 0 entonces de strttime se reseta = 0
    
  } 
    
}


//------------------------------------------------------------
void particle::addAttractionForceParticles(particle * p, float radius, float scale){
	
  // ----------- (1) make a vector of where this particle p is: 

  glm::vec2 posOfForce;
  posOfForce = glm::vec2(p->pos.x, p->pos.y);	
	
  // ----------- (2) calculate the difference & length 

  glm::vec2 diff = pos - posOfForce;
  float length = glm::length(diff);
  
  // ----------- (3) check close enough
	
  bool bAmCloseEnough = true;
  if (radius > 0){
    if (length > radius){
      bAmCloseEnough = false;
    }
  }
	
  // ----------- (4) if so, update force
    
  if (bAmCloseEnough == true){
    float pct = 1 - (length / radius);  // stronger on the inside
    glm::vec2 nDiff = glm::normalize(diff);
    frc.x = frc.x - nDiff.x * scale * pct;
    frc.y = frc.y - nDiff.y * scale * pct;
    p->frc.x = p->frc.x + nDiff.x * scale * pct;
    p->frc.y = p->frc.y + nDiff.y * scale * pct;
    }
	
}


//--------------------------------------

void particle::addReturnForce(){
  
  glm::vec2 returnForce = initialPos - pos;
  pos += returnForce * 0.01;
}

//---------------------------

/*
void particle::addRepulsion(glm::vec2 repulsionPoint){





}
*/
//-----------------------------

void particle::particleReset(){

  uniqueVal = ofRandom(-10000, 10000);
	
  pos.x = ofRandomWidth();
  pos.y = ofRandomHeight();

	
  vel.x = ofRandom(-3.9, 3.9);
  vel.y = ofRandom(-3.9, 3.9);

	
  frc   = glm::vec3(0,0,0);	
  scale = ofRandom(0.5, 1.0);
  drag  = ofRandom(0.95, 0.998);	
}



//-----------------------------------------------------------

void particle::addRandomForce(float x, float y, float radius, float scale){

  // ----------- (1) make a vector of where this particle p is: 

  glm::vec2 posOfForce;
  posOfForce = glm::vec2(x, y);	
	
  // ----------- (2) calculate the difference & length 

  glm::vec2 diff = pos - posOfForce;
  float length = glm::length(diff);
  
  // ----------- (3) check close enough
	
  bool bAmCloseEnough = true;
  if (radius > 0){
    if (length > radius){
      bAmCloseEnough = false;
    }
  }
	
  // ----------- (4) if so, update force


  
  if (bAmCloseEnough){

    float uniqueVal = ofRandom(-10000, 10000);
    frc.x = ofSignedNoise(uniqueVal, pos.y * scale, ofGetElapsedTimef()*0.001);
    frc.y = ofSignedNoise(uniqueVal, pos.x * scale, ofGetElapsedTimef()*0.001);
    //    vel += frc * 0.04;

  }
}




//--------------------------------------------------------

void particle::addGravityForce(float force, float mass){

  frc.x = frc.x;
  frc.y += force * mass;
  

}

//------------------------------------------------------------
void particle::addDampingForce(){
	
  // the usual way to write this is  vel *= 0.99
  // basically, subtract some part of the velocity 
  // damping is a force operating in the oposite direction of the 
  // velocity vector
	
  frc.x = frc.x - vel.x * damping;
  frc.y = frc.y - vel.y * damping;
}

//-------------------------------------

void particle::addAngularMotion(){

  /*
    
  float aAcceleration = 0.01;
  float angle = std::atan2(vel.y, vel.x);//rads

  float aRotationX = 0;
  float aRotationY = 0;

  aRotationX += vel.x;
  aRotationY += vel.y;
  
  aAcceleration = ofClamp(aAcceleration, -0.1, 0.1);
  angle += aAcceleration;

  aRotationX = ofClamp(aRotationX, -0.1, 0.1);
  aRotationY = ofClamp(aRotationY, -0.1, 0.1);
  
  ofPushMatrix();

  ofRotateRad(aRotationX, 1, 0, 0);
  ofRotateRad(aRotationY, 0, 1, 0);
  

  ofPopMatrix();

  */

  float targetAngle = ofRadToDeg(atan2(vel.y, vel.x));

  float aRotationX = 0;
  float aRotationY = 0;

  aRotationX += vel.x;
  aRotationY += vel.y;
 
  float torque = targetAngle - angle;
  torque *= 0.1;
  angularVelocity += torque;
  angularVelocity *= 0.99;

  // Update the angle based on angular velocity
  angle += angularVelocity;

  aRotationX = ofClamp(aRotationX, -0.1, 0.1);
  aRotationY = ofClamp(aRotationY, -0.1, 0.1);
  
  ofPushMatrix();

  ofRotate(aRotationX, 1, 0, 0);
  ofRotate(aRotationY, 0, 1, 0);
 
  ofPopMatrix();


	
}


//------------------------------------------------------------
void particle::setInitialCondition(float px, float py, float vx, float vy){

  pos = glm::vec2(px, py);
  initialPos = pos;
  vel = glm::vec2(vx, vy);
}

//------------------------------------------------------------
void particle::update(){	
  vel += frc;
  pos += vel;
}

//------------------------------------------------------------


void particle::draw(){

  // ofSetColor(color);
  //ofDrawCircle(pos.x, pos.y, size);
  image.draw(pos.x, pos.y);
}

//-----------------------------------

void particle::loadImage(std::string str){

 int total = dir.listDir(str);

  ofLog() << total;
  
  string newPath = dir.getPath((int)ofRandom(total));
  image.load(newPath);
  image.resize(image.getWidth() * size, image.getHeight() * size); //change 

}


//------------------------------------------------------------
void particle::bounceOffWalls(){
	
  // sometimes it makes sense to damped, when we hit
  bool bDampedOnCollision = true;
  bool bDidICollide = false;
	
  // what are the walls
  float minx = 0;
  float miny = 0;
  float maxx = ofGetWidth();
  float maxy = ofGetHeight();
	
  if (pos.x > maxx){
    pos.x = maxx; // move to the edge, (important!)
    vel.x *= -1;
    bDidICollide = true;
  } else if (pos.x < minx){
    pos.x = minx; // move to the edge, (important!)
    vel.x *= -1;
    bDidICollide = true;
  }
  
  if (pos.y > maxy){
    pos.y = maxy; // move to the edge, (important!)
    vel.y *= -1;
    bDidICollide = true;
  } else if (pos.y < miny){
    pos.y = miny; // move to the edge, (important!)
    vel.y *= -1;
    bDidICollide = true;
  }
  
  if (bDidICollide == true && bDampedOnCollision == true){
    vel *= 0.5;
  }
  
}
