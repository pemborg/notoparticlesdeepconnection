
#include "squareParticle.h"

void squareParticle::loadImage(std::string str){

  int total = dir.listDir(str);

  ofLog() << total;
  
  string newPath = dir.getPath((int)ofRandom(total));
  image.load(newPath);
  image.resize(image.getWidth() * size, image.getHeight() * size); //change 
}

void squareParticle::draw(){

  image.draw(pos.x, pos.y);
  
}
